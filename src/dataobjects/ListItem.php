<?php

namespace Hestec\ElementalExtensions\Dataobjects;

use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
use SilverStripe\Forms\TextField;
use SilverStripe\ORM\DataObject;
use Hestec\ElementalExtensions\Elements\ElementList;
use SilverStripe\Forms\FieldList;
use SilverStripe\Security\Permission;

class ListItem extends DataObject {

    private static $table_name = 'HestecElementListItem';

    private static $singular_name = 'ListItem';
    private static $plural_name = 'ListItems';

    private static $db = [
        'Title' => 'Varchar(255)',
        'SubTitle' => 'Varchar(255)',
        'Content' => 'HTMLText',
        'Sort' => 'Int'
    ];

    /*private static $defaults = array(
        'BorderRight' => true
    );*/

    private static $has_one = [
        'ElementList' => ElementList::class
    ];

    /*private static $owns = [
        'Image',
        'Icon'
    ];*/

    private static $summary_fields = [
        'Title' => 'Title'
    ];

    private static $default_sort = 'Sort';

    public function getCMSFields()
    {
        $TitleField = TextField::create('Title', "Title");
        $SubTitleField = TextField::create('SubTitle', "SubTitle");
        $ContentField = HTMLEditorField::create('Content', "Content");

        return new FieldList(
            $TitleField,
            $SubTitleField,
            $ContentField
        );

    }

    public function canView($member = null)
    {
        return Permission::check('CMS_ACCESS_CMSMain', 'any', $member);
    }

    public function canEdit($member = null)
    {
        return Permission::check('CMS_ACCESS_CMSMain', 'any', $member);
    }

    public function canDelete($member = null)
    {
        return Permission::check('CMS_ACCESS_CMSMain', 'any', $member);
    }

    public function canCreate($member = null, $context = [])
    {
        return Permission::check('CMS_ACCESS_CMSMain', 'any', $member);
    }

}
