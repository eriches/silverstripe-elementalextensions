<?php

namespace Hestec\ElementalExtensions\Dataobjects;

use SilverStripe\AssetAdmin\Forms\UploadField;
use SilverStripe\Forms\CheckboxField;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
use SilverStripe\Forms\TextField;
use SilverStripe\ORM\DataObject;
use Hestec\ElementalExtensions\Elements\ElementColumns;
use SilverStripe\Forms\FieldList;
use SilverStripe\Assets\Image;
use SilverStripe\Forms\DropdownField;
use SilverStripe\Forms\NumericField;
use SilverStripe\CMS\Model\SiteTree;
use SilverStripe\Forms\TreeDropdownField;
use SilverStripe\View\ArrayData;
use SilverStripe\Security\Permission;

class ContentColumn extends DataObject {

    private static $table_name = 'HestecElementContentColumn';

    private static $singular_name = 'ContentColumn';
    private static $plural_name = 'ContentColumns';

    private static $db = [
        'Title' => 'Varchar(255)',
        'AnchorTag' => 'Varchar(255)',
        'AddAnchorTag' => 'Boolean',
        'Content' => 'HTMLText',
        'Card' => 'Boolean',
        'BorderRight' => 'Boolean',
        'CenterIcon' => 'Boolean',
        'IconScaleMaxWidth' => 'Int',
        'IconScaleMaxHeight' => 'Int',
        'IconPad' => 'Boolean',
        'IconPadColor' => 'Varchar(6)',
        'CenterImage' => 'Boolean',
        'ImageScaleMaxWidth' => 'Int',
        'ImageScaleMaxHeight' => 'Int',
        'ImagePad' => 'Boolean',
        'ImagePadColor' => 'Varchar(6)',
        'CenterHeader' => 'Boolean',
        'CenterContent' => 'Boolean',
        'Header' => "Enum('h2,h3,h4','h2')",
        'HSize'=> "Enum('normal,h-smaller,h-small','normal')",
        'UnderlineTitle' => 'Varchar(255)',
        'LinkExtern' => 'Varchar(255)',
        'LinkText' => 'Varchar(255)',
        'LinkTitle'  => 'Varchar(255)',
        'LinkAlign' => "Enum('LEFT,CENTER,RIGHT','LEFT')",
        'LinkFaIcon' => 'Varchar(25)',
        'LinkFaIconPos' => "Enum('LEFT,RIGHT','LEFT')",
        'LinkButton' => 'Boolean',
        'LinkNoFollow' => 'Boolean',
        'Cols' => "Enum('1,2,3,4,5,6,7,8,9,10,11,12','')",
        'Sort' => 'Int'
    ];

    private static $defaults = array(
        'BorderRight' => true
    );

    private static $has_one = [
        'ElementColumns' => ElementColumns::class,
        'Image' => Image::class,
        'Icon' => Image::class,
        'LinkIntern' => SiteTree::class
    ];

    private static $owns = [
        'Image',
        'Icon'
    ];

    private static $summary_fields = [
        'Title' => 'Title',
        'Card.Nice' => 'Card',
        'BorderRight.Nice' => 'BorderRight'
    ];

    private static $default_sort = 'Sort';

    public function getCMSFields()
    {
        $TitleField = TextField::create('Title', "Title");
        $AnchorTagField = TextField::create('AnchorTag', _t('Element.ANCHORTAG', "Anchor tag"));
        $AddAnchorTagField = CheckboxField::create('AddAnchorTag', _t('Element.ADDANCHORTAG', "Add anchor tag"));
        $AddAnchorTagField->setDescription(_t('Element.ADDANCHORTAG_DESCRIPTION', "Add an anchor tag for the index, you can adjust the tag under settings."));
        $ContentField = HTMLEditorField::create('Content', "Content");
        $CardField = CheckboxField::create('Card', "Card");
        $BorderRightField = CheckboxField::create('BorderRight', "BorderRight");
        $BorderRightField->setDescription("The last column never had a right border.");

        $ImageField = UploadField::create('Image', "Image");
        $IconField = UploadField::create('Icon', "Icon");

        $CenterIconField = CheckboxField::create('CenterIcon', "CenterIcon");
        $CenterImageField = CheckboxField::create('CenterImage', "CenterImage");
        $CenterHeaderField = CheckboxField::create('CenterHeader', "CenterHeader");
        $CenterContentField = CheckboxField::create('CenterContent', "CenterContent");
        $IconScaleMaxWidthField = NumericField::create('IconScaleMaxWidth', "IconScaleMaxWidth");
        $IconScaleMaxHeightField = NumericField::create('IconScaleMaxHeight', "IconScaleMaxHeight");
        $IconPadField = CheckboxField::create('IconPad', "IconPad");
        $IconPadField->setDescription("Pad icon instead of scale, only when width and height are filled.");
        $IconPadColorField = TextField::create('IconPadColor', "IconPadColor");
        $IconPadColorField->setDescription("When empty the color is FFFFFF for .JPG and transparent for .PNG.");
        $ImageScaleMaxWidthField = NumericField::create('ImageScaleMaxWidth', "ImageScaleMaxWidth");
        $ImageScaleMaxHeightField = NumericField::create('ImageScaleMaxHeight', "ImageScaleMaxHeight");
        $ImagePadField = CheckboxField::create('ImagePad', "ImagePad");
        $ImagePadField->setDescription("Pad image instead of scale, only when width and height are filled.");
        $ImagePadColorField = TextField::create('ImagePadColor', "ImagePadColor");
        $ImagePadColorField->setDescription("When empty the color is FFFFFF for .JPG and transparent for .PNG.");

        $HeaderField = DropdownField::create('Header', "Header", $this->dbObject('Header')->enumValues());
        $HSizeField = DropdownField::create('HSize', "HSize", $this->dbObject('HSize')->enumValues());
        $UnderlineTitleField = CheckboxField::create('UnderlineTitle', "hr below title");

        $LinkInternField = TreeDropdownField::create('LinkInternID', "LinkIntern", SiteTree::class);
        $LinkExternField = TextField::create('LinkExtern', "LinkExtern");
        $LinkTextField = TextField::create('LinkText', "LinkText");
        $LinkTitleField = TextField::create('LinkTitle', "LinkTitle");
        $LinkAlignField = DropdownField::create('LinkAlign', "LinkAlign", $this->dbObject('LinkAlign')->enumValues());
        $LinkFaIconField = TextField::create('LinkFaIcon', "LinkFaIcon");
        $LinkFaIconPosField = DropdownField::create('LinkFaIconPos', "LinkFaIconPos", $this->dbObject('LinkFaIconPos')->enumValues());
        $LinkButtonField = CheckboxField::create('LinkButton', "LinkButton");
        $LinkNoFollowField = CheckboxField::create('LinkNoFollow', "LinkNoFollow");

        $ColsField = DropdownField::create('Cols', "Cols", $this->dbObject('Cols')->enumValues());
        $ColsField->setEmptyString("(select)");
        $ColsField->setDescription("Optional.");

        return new FieldList(
            $TitleField,
            $AddAnchorTagField,
            $AnchorTagField,
            $ContentField,
            $CardField,
            $BorderRightField,
            $ColsField,
            $ImageField,
            $ImageScaleMaxWidthField,
            $ImageScaleMaxHeightField,
            $ImagePadField,
            $ImagePadColorField,
            $IconField,
            $IconScaleMaxWidthField,
            $IconScaleMaxHeightField,
            $IconPadField,
            $IconPadColorField,
            $CenterIconField,
            $CenterImageField,
            $CenterHeaderField,
            $CenterContentField,
            $HeaderField,
            $HSizeField,
            $UnderlineTitleField,
            $LinkInternField,
            $LinkExternField,
            $LinkTextField,
            $LinkTitleField,
            $LinkAlignField,
            $LinkFaIconField,
            $LinkFaIconPosField,
            $LinkButtonField,
            $LinkNoFollowField
        );

    }

    public function onBeforeWrite() {

        if ($this->owner->AddAnchorTag == true){

            if (strlen($this->owner->AnchorTag) < 2){

                $this->owner->AnchorTag = strip_tags($this->owner->Title);

            }

            $this->owner->AnchorTag = SiteTree::create()->generateURLSegment($this->owner->AnchorTag);

        }

        parent::onBeforeWrite();

    }

    public function IconSized(){

        if ($this->IconScaleMaxWidth > 0 && $this->IconScaleMaxHeight > 0){

            if ($this->IconPad == true){

                $padcolor = "FFFFFF";
                if (strlen($this->IconPadColor) === 6){
                    $padcolor = $this->IconPadColor;
                }

                return $this->Icon()->Pad($this->IconScaleMaxWidth,$this->IconScaleMaxHeight, $padcolor, 100);

            }
            return $this->Icon()->FitMax($this->IconScaleMaxWidth,$this->IconScaleMaxHeight);

        }elseif ($this->IconScaleMaxWidth > 0) {

            return $this->Icon()->ScaleMaxWidth($this->IconScaleMaxWidth);

        }elseif ($this->IconScaleMaxHeight > 0) {

            return $this->Icon()->ScaleMaxWidth($this->IconScaleMaxHeight);

        }

        return $this->Icon();

    }

    public function ImageSized(){

        if ($this->ImageScaleMaxWidth > 0 && $this->ImageScaleMaxHeight > 0){

            if ($this->ImagePad == true){

                $padcolor = "FFFFFF";
                if (strlen($this->ImagePadColor) === 6){
                    $padcolor = $this->ImagePadColor;
                }

                return $this->Image()->Pad($this->ImageScaleMaxWidth,$this->ImageScaleMaxHeight, $padcolor, 100);

            }
            return $this->Image()->FitMax($this->ImageScaleMaxWidth,$this->ImageScaleMaxHeight);

        }elseif ($this->ImageScaleMaxWidth > 0) {

            return $this->Image()->ScaleMaxWidth($this->ImageScaleMaxWidth);

        }elseif ($this->ImageScaleMaxHeight > 0) {

            return $this->Image()->ScaleMaxWidth($this->ImageScaleMaxHeight);

        }

        return $this->Image();

    }

    public function LinkUrlType(){

        if ($this->LinkIntern()->ID > 0){

            $output = [
                'Url' => $this->LinkIntern()->AbsoluteLink(),
                'Type' => "INTERN"
            ];

            return ArrayData::create($output);

        }elseif (filter_var($this->LinkExtern, FILTER_VALIDATE_URL)) {

            $output = [
                'Url' => $this->LinkExtern,
                'Type' => "EXTERN"
            ];

            return ArrayData::create($output);

        }
        return false;

    }

    public function canView($member = null)
    {
        return Permission::check('CMS_ACCESS_CMSMain', 'any', $member);
    }

    public function canEdit($member = null)
    {
        return Permission::check('CMS_ACCESS_CMSMain', 'any', $member);
    }

    public function canDelete($member = null)
    {
        return Permission::check('CMS_ACCESS_CMSMain', 'any', $member);
    }

    public function canCreate($member = null, $context = [])
    {
        return Permission::check('CMS_ACCESS_CMSMain', 'any', $member);
    }

}
