<?php

namespace Hestec\ElementalExtensions\Dataobjects;

use SilverStripe\Forms\CheckboxField;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
use SilverStripe\Forms\TextField;
use SilverStripe\ORM\DataObject;
use Hestec\ElementalExtensions\Elements\ElementTimeline;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\DropdownField;
use SilverStripe\CMS\Model\SiteTree;
use SilverStripe\Forms\TreeDropdownField;
use SilverStripe\View\ArrayData;
use SilverStripe\Security\Permission;

class TimelineItem extends DataObject {

    private static $table_name = 'HestecElementTimelineItem';

    private static $singular_name = 'TimelineItem';
    private static $plural_name = 'TimelineItems';

    private static $db = [
        'Title' => 'Varchar(255)',
        'DateText' => 'Varchar(255)',
        'Content' => 'HTMLText',
        'LinkExtern' => 'Varchar(255)',
        'LinkText' => 'Varchar(255)',
        'LinkTitle'  => 'Varchar(255)',
        'LinkAlign' => "Enum('LEFT,CENTER,RIGHT','LEFT')",
        'LinkFaIcon' => 'Varchar(25)',
        'LinkFaIconPos' => "Enum('LEFT,RIGHT','LEFT')",
        'LinkButton' => 'Boolean',
        'Highlighted' => 'Boolean',
        'Muted' => 'Boolean',
        'Sort' => 'Int'
    ];

    /*private static $defaults = array(
        'BorderRight' => true
    );*/

    private static $has_one = [
        'ElementTimeline' => ElementTimeline::class,
        'LinkIntern' => SiteTree::class
    ];

    /*private static $owns = [
        'Image',
        'Icon'
    ];*/

    private static $summary_fields = [
        'Title' => 'Title',
        'DateText' => 'Date'
    ];

    private static $default_sort = 'Sort';

    public function getCMSFields()
    {
        $TitleField = TextField::create('Title', "Title");
        $DateTextField = TextField::create('DateText', "DateText");
        $ContentField = HTMLEditorField::create('Content', "Content");
        $HighlightedField = CheckboxField::create('Highlighted', "Highlighted");

        $LinkInternField = TreeDropdownField::create('LinkInternID', "LinkIntern", SiteTree::class);
        $LinkExternField = TextField::create('LinkExtern', "LinkExtern");
        $LinkTextField = TextField::create('LinkText', "LinkText");
        $LinkTitleField = TextField::create('LinkTitle', "LinkTitle");
        $LinkAlignField = DropdownField::create('LinkAlign', "LinkAlign", $this->dbObject('LinkAlign')->enumValues());
        $LinkFaIconField = TextField::create('LinkFaIcon', "LinkFaIcon");
        $LinkFaIconPosField = DropdownField::create('LinkFaIconPos', "LinkFaIconPos", $this->dbObject('LinkFaIconPos')->enumValues());
        $LinkButtonField = CheckboxField::create('LinkButton', "LinkButton");

        return new FieldList(
            $TitleField,
            $HighlightedField,
            $DateTextField,
            $ContentField,
            $LinkInternField,
            $LinkExternField,
            $LinkTextField,
            $LinkTitleField,
            $LinkAlignField,
            $LinkFaIconField,
            $LinkFaIconPosField,
            $LinkButtonField
        );

    }

    public function LinkUrlType(){

        if ($this->LinkIntern()->ID > 0){

            $output = [
                'Url' => $this->LinkIntern()->AbsoluteLink(),
                'Type' => "INTERN"
            ];

            return ArrayData::create($output);

        }elseif (filter_var($this->LinkExtern, FILTER_VALIDATE_URL)) {

            $output = [
                'Url' => $this->LinkExtern,
                'Type' => "EXTERN"
            ];

            return ArrayData::create($output);

        }
        return false;

    }

    public function canView($member = null)
    {
        return Permission::check('CMS_ACCESS_CMSMain', 'any', $member);
    }

    public function canEdit($member = null)
    {
        return Permission::check('CMS_ACCESS_CMSMain', 'any', $member);
    }

    public function canDelete($member = null)
    {
        return Permission::check('CMS_ACCESS_CMSMain', 'any', $member);
    }

    public function canCreate($member = null, $context = [])
    {
        return Permission::check('CMS_ACCESS_CMSMain', 'any', $member);
    }

}
