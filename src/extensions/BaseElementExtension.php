<?php

namespace Hestec\ElementalExtensions\Extensions;

use SilverStripe\ORM\DataExtension;
use SilverStripe\Control\Controller;
use SilverStripe\Assets\Image;
use SilverStripe\AssetAdmin\Forms\UploadField;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\DropdownField;
use SilverStripe\Forms\CheckboxField;
use SilverStripe\Forms\LiteralField;
use Heyday\ColorPalette\Fields\ColorPaletteField;
use SilverStripe\Core\Config\Config;
use SilverStripe\Forms\OptionsetField;
use SilverStripe\Forms\TextField;
use SilverStripe\CMS\Model\SiteTree;

class BaseElementExtension extends DataExtension {

    private static $db = array(
        'HiddenXS' => 'Boolean',
        'HiddenSM' => 'Boolean',
        'HiddenMD' => 'Boolean',
        'HiddenLG' => 'Boolean',
        'HiddenXL' => 'Boolean',
        'PaddingTop' => "Enum('0,1,2,3,4,5,6','5')",
        'PaddingBottom' => "Enum('0,1,2,3,4,5,6','5')",
        'BgColor' => 'Int',
        'HSize'=> "Enum('normal,h-smaller,h-small,h-larger,h-large','normal')",
        'HHlt' => "Enum('above,under','')",
        'TitleCentered' => 'Boolean',
        'AnchorTag' => 'Varchar(255)',
        'AddAnchorTag' => 'Boolean',
        'ContainerWidth' => "Enum('container,container-fluid,container-lg,container-md','')",
        'LeftLine' => 'Boolean',
        'BottomLine' => 'Boolean',
        'BgImgAuto' => 'Boolean'
    );

    private static $has_one = array(
        'BackgroundImage' => Image::class
    );

    private static $owns = [
        'BackgroundImage'
    ];

    private static $defaults = array(
        'PaddingTop' => 5,
        'PaddingBottom' => 5
    );


    public function updateCMSFields(FieldList $fields)
    {

        $Attention = _t('Element.ATTENTION', "<p><strong>Attention</strong>: Not all options and features in this element may apply to this website. In that case, the option or function will therefore make no difference.</p>");

        $AttentionMainField = LiteralField::create('AttentionMain', $Attention);
        $AttentionSettingsField = LiteralField::create('AttentionSettings', $Attention);


        $HSizeSource = array(
            'h-large' => _t('Element.LARGE', "large"),
            'h-larger' => _t('Element.LARGER', "larger"),
            'normal' => _t('Element.NORMAL', "normal"),
            'h-smaller' => _t('Element.SMALLER', "smaller"),
            'h-small' => _t('Element.SMALL', "small")
        );

        $BgColorSource = array(
            '#fff',
            '#000'
        );
        if (is_array(Config::inst()->get('BrandColors', 'ElementBgColors'))){
            $BgColorSource = Config::inst()->get('BrandColors', 'ElementBgColors');
        }

        $colorextra = '';
        foreach ($BgColorSource as $color){

            $colorextra .= $color.' = <span style="background-color:'.$color.';border:1px solid #000;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;&nbsp;&nbsp;';

        }

        $BackgroundImageField = UploadField::create('BackgroundImage', _t('Element.BACKGROUNDIMAGE', "Background image"));
        $BackgroundImageField->setAllowedMaxFileNumber(1);
        $BackgroundImageField->setFolderName("bg");

        $BgImgAutoField = CheckboxField::create('BgImgAuto', _t('Element.BGIMGAUTO', "Background image auto size"));
        $BgImgAutoField->setDescription(_t('Element.BGIMGAUTO_DESCRIPTION', "Auto size the background image, for example see the homepage of Reisdesk."));

        $HSizeField = OptionsetField::create('HSize', _t('Element.TITLE_SIZE', "Title size"), $HSizeSource);
        $HSizeField->addExtraClass("layout-horizontal");

        $HHltField = DropdownField::create('HHlt', "H Hlt", $this->owner->dbObject('HHlt')->enumValues());
        $HHltField->setEmptyString("none");
        $HHltField->setDescription("Add a short line above or under the H title.");

        $TitleCenteredField = CheckboxField::create('TitleCentered' , _t('Element.TITLECENTERED', "Title centered"));
        $AnchorTagField = TextField::create('AnchorTag', _t('Element.ANCHORTAG', "Anchor tag"));

        $AddAnchorTagField = CheckboxField::create('AddAnchorTag', _t('Element.ADDANCHORTAG', "Add anchor tag"));
        $AddAnchorTagField->setDescription(_t('Element.ADDANCHORTAG_DESCRIPTION', "Add an anchor tag for the index, you can adjust the tag under settings."));

        $ContainerWidthField = DropdownField::create('ContainerWidth', _t('Element.ELEMENT_WIDTH', "Container width"), $this->owner->dbObject('ContainerWidth')->enumValues());
        $ContainerWidthField->setEmptyString("(page default)");
        $LeftLineField = CheckboxField::create('LeftLine', _t('Element.LEFTLINE', "Left line"));
        $LeftLineField->setDescription(_t('Element.LEFTLINE_DESCRIPTION', "Vertical line left of text (only with container-lg)."));
        $BottomLineField = CheckboxField::create('BottomLine', _t('Element.BOTTOMLINE', "Bottom line"));

        $HiddenXSField = CheckboxField::create('HiddenXS', _t('Element.HiddenXS', "hide on xs"));
        $HiddenSMField = CheckboxField::create('HiddenSM', _t('Element.HiddenSM', "hide on sm"));
        $HiddenMDField = CheckboxField::create('HiddenMD', _t('Element.HiddenMD', "hide on md"));
        $HiddenLGField = CheckboxField::create('HiddenLG', _t('Element.HiddenLG', "hide on lg"));
        $HiddenXLField = CheckboxField::create('HiddenXL', _t('Element.HiddenXL', "hide on xl"));

        $PaddingTopField = DropdownField::create('PaddingTop', _t('Element.PADDINGTOP', "Padding top"), $this->owner->dbObject('PaddingTop')->enumValues());
        $PaddingBottomField = DropdownField::create('PaddingBottom', _t('Element.PADDINGBOTTOM', "Padding bottom"), $this->owner->dbObject('PaddingBottom')->enumValues());
        $BgColorField = ColorPaletteField::create('BgColor', _t('Element.BACKGROUNDCOLOR', "Background color"), $BgColorSource);
        $BgColorExtraField = LiteralField::create('BgColorExtra', '<p>'.$colorextra.'</p>');

        $fields->addFieldToTab('Root.Settings', $AttentionSettingsField, 'ExtraClass');
        $fields->addFieldToTab('Root.Settings', $ContainerWidthField);
        $fields->addFieldToTab('Root.Settings', $LeftLineField);
        $fields->addFieldToTab('Root.Settings', $BottomLineField);
        $fields->addFieldToTab('Root.Settings', $HHltField);
        $fields->addFieldToTab('Root.Settings', $HiddenXSField);
        $fields->addFieldToTab('Root.Settings', $HiddenSMField);
        $fields->addFieldToTab('Root.Settings', $HiddenMDField);
        $fields->addFieldToTab('Root.Settings', $HiddenLGField);
        $fields->addFieldToTab('Root.Settings', $HiddenXLField);
        $fields->addFieldToTab('Root.Settings', $PaddingTopField);
        $fields->addFieldToTab('Root.Settings', $PaddingBottomField);
        $fields->addFieldToTab('Root.Settings', $BackgroundImageField);
        $fields->addFieldToTab('Root.Settings', $BgImgAutoField);
        $fields->addFieldToTab('Root.Settings', $BgColorField);
        $fields->addFieldToTab('Root.Settings', $BgColorExtraField);
        $fields->addFieldToTab('Root.Settings', $AnchorTagField);
        $fields->addFieldToTab('Root.Main', $AttentionMainField, 'HSize');
        $fields->addFieldToTab('Root.Main', $HSizeField, 'TitleCentered');
        $fields->addFieldToTab('Root.Main', $TitleCenteredField, 'AddAnchorTag');
        $fields->addFieldToTab('Root.Main', $AddAnchorTagField, 'Title');

        return $fields;

    }

    public function onBeforeWrite() {

        if ($this->owner->AddAnchorTag == true){

            if (!$this->owner->AnchorTag || ($this->owner->AnchorTag && strlen($this->owner->AnchorTag) < 2)){

                $this->owner->AnchorTag = strip_tags($this->owner->Title);

            }

            $this->owner->AnchorTag = SiteTree::create()->generateURLSegment($this->owner->AnchorTag);

        }

        if ($this->owner->Content && strlen($this->owner->Content) > 0){
            $this->owner->Content = preg_replace('/<p>((?=\[NOP_).*?])<\/p>/m', '$1', $this->owner->Content);
        }
        if ($this->owner->HTML && strlen($this->owner->HTML) > 0) {
            $this->owner->HTML = preg_replace('/<p>((?=\[NOP_).*?])<\/p>/m', '$1', $this->owner->HTML);
        }

        parent::onBeforeWrite();

    }

    /**
     * @return current page Controller
     */
    public function TopController()
    {
        return (Controller::has_curr()) ? Controller::curr() : null;
    }

    // No partial cache if logged in or in dev (in hestec-tools ExtraPageControlsExtension.php)
    public function noCache()
    {
        return $this->TopController()->noCache();
    }

    public function InsertAnchorTag(){

        if (!empty($this->owner->AnchorTag) && $this->owner->AddAnchorTag == true){

            return true;

        }
        return false;

    }

}
