<?php

namespace Hestec\ElementalExtensions\Extensions;

use SilverStripe\ORM\DataExtension;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\CheckboxField;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;

class ElementalPageExtension extends DataExtension {

    private static $db = array(
        'EnableContent' => 'Boolean'
    );

    private static $defaults = array(
        'EnableContent' => 1,
    );

    public function updateSettingsFields(FieldList $fields) {
        //$fields = parent::getCMSFields();

        $EnableContentField = CheckboxField::create('EnableContent', "EnableContent");
        $EnableContentField->setDescription("place Content field back after it is hidden by Elemental");

        $fields->addFieldToTab("Root.Settings", new CheckBoxField('ShowInFooter', _t("SiteTree.SHOWINFOOTER","Show in footer?")), 'ShowInSearch');
        $fields->addFieldToTab("Root.Settings", $EnableContentField);

        return $fields;
    }

    public function updateCMSFields(FieldList $fields)
    {

        if ($this->owner->EnableContent == true) {

            // place Content field back after it is hidden by Elemental
            $ContentField = HTMLEditorField::create('Content', 'Content');
            $ContentField->addExtraClass('stacked');

            $fields->addFieldsToTab('Root.Main', array(
                $ContentField
            ), 'Content');

        }

    }

}
