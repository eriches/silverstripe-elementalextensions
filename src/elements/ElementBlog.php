<?php

namespace Hestec\ElementalExtensions\Elements;

use DNADesign\Elemental\Models\BaseElement;
use SilverStripe\Blog\Model\BlogPost;
use SilverStripe\Blog\Model\BlogCategory;
use SilverStripe\Forms\DropdownField;

class ElementBlog extends BaseElement
{

    private static $table_name = 'HestecElementBlog';

    private static $singular_name = 'Blog';

    private static $plural_name = 'Blogs';

    private static $description = 'Blog';

    private static $icon = 'font-icon-pencil';

    private static $db = [
        'TitleCenter' => 'Boolean',
        'Container' => "Enum('container,container-fluid','container')",
    ];

    private static $has_one = array(
        'BlogCategory' => BlogCategory::class
    );

    public function getCMSFields()
    {

        $this->beforeUpdateCMSFields(function ($fields) {

            $BlogCategorySource = BlogCategory::get();

            $BlogCategoryField = DropdownField::create('BlogCategoryID', "BlogCategory", $BlogCategorySource);
            $BlogCategoryField->setEmptyString("- all categoeries -");

            $ContainerField = DropdownField::create('Container', 'Container', $this->dbObject('Container')->enumValues());
            $ContainerField->setDescription("container-fluid returns 4 blogs, container returns 3 blogs");

            $fields->addFieldsToTab('Root.Main', Array(
                $BlogCategoryField
            ));
        });

        $fields = parent::getCMSFields();

        return $fields;
    }


    /*public function PageController(){
        return Controller::curr();
    }

    public function Top()
    {
        return Controller::curr();
    }*/


    public function getLatestBlogs() {

        $blogs = 3;
        if ($this->Container == "container-fluid"){
            $blogs = 4;
        }

        if ($this->BlogCategoryID){
            return BlogPost::get()->filter(['Categories.ID' => $this->BlogCategoryID])->limit($blogs);
        }
        return BlogPost::get()->sort('PublishDate DESC')->limit($blogs);

    }

    public function getType()
    {
        return 'Blog';
    }

}