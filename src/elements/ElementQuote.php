<?php

namespace Hestec\ElementalExtensions\Elements;

use DNADesign\Elemental\Models\BaseElement;
use SilverStripe\Forms\DropdownField;
use SilverStripe\Forms\TextField;

class ElementQuote extends BaseElement
{

    private static $table_name = 'HestecElementQuote';

    private static $singular_name = 'Quote';

    private static $plural_name = 'Quotes';

    private static $description = 'Quote';

    private static $icon = 'font-icon-comment';

    private static $db = [
        'QuoteFooter' => 'Varchar(255)',
        'QuoteType' => "Enum('emphasize,quote', 'emphasize')",
        'Align' => "Enum('left,center,right', 'left')",
        'Container' => "Enum('container,container-fluid','container')"
    ];

    public function getCMSFields()
    {

        $this->beforeUpdateCMSFields(function ($fields) {

            $QuoteFooterField = TextField::create('QuoteFooter', _t('Element.QUOTEFOOTER', "Quote footer"));
            $QuoteFooterField->setDescription(_t('Element.QUOTEFOOTER_CITE', 'Optional: set a source title with < cite title="optional title" >...< /cite >'));

            $QuoteTypeField = DropdownField::create('QuoteType', _t('Element.QUOTETYPE', "QuoteType"), $this->dbObject('QuoteType')->enumValues());
            $AlignField = DropdownField::create('Align', _t('Element.ALIGN', "Align"), $this->dbObject('Align')->enumValues());

            $ContainerField = DropdownField::create('Container', 'Container', $this->dbObject('Container')->enumValues());

            $fields->addFieldsToTab('Root.Main', Array(
                $QuoteFooterField,
                $AlignField,
                $ContainerField,
                $QuoteTypeField
            ));
        });

        $fields = parent::getCMSFields();

        return $fields;
    }

    public function getType()
    {
        return 'Quote';
    }
}