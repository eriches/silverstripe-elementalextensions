<?php

namespace Hestec\ElementalExtensions\Elements;

use DNADesign\Elemental\Models\BaseElement;
use SilverStripe\Forms\CheckboxField;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
use SilverStripe\Forms\GridField\GridField;
use SilverStripe\Forms\GridField\GridFieldConfig_RecordEditor;
use Symbiote\GridFieldExtensions\GridFieldOrderableRows;
use SilverStripe\Forms\DropdownField;
use Hestec\ElementalExtensions\Dataobjects\ListItem;

class ElementList extends BaseElement
{

    private static $table_name = 'HestecElementList';

    private static $singular_name = 'List';

    private static $plural_name = 'Lists';

    private static $description = 'Element with list';

    //private static $icon = 'timeline-icon';
    private static $icon = 'font-icon-list';

    private static $db = [
        'Content' => 'HTMLText',
        'ContentBelow' => 'HTMLText',
        'ListType' => "Enum('ul,ol','')",
        'ListIcon' => "Enum('dot,check,arrow,thumbs-up,hand,plus-cirlce,angle-right','')",
        'NumberStyle' => "Enum('number-1,number-2,number-3','')",
        'TitleCenter' => 'Boolean',
        'ListCenter' => 'Boolean'
    ];

    private static $has_many = array(
        'ListItems' => ListItem::class
    );

    private static $inline_editable = false;

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();

        $ContentField = HTMLEditorField::create('Content', "Content");
        $ContentField->setRows(5);
        $ContentBelowField = HTMLEditorField::create('ContentBelow', "ContentBelow");
        $ContentBelowField->setRows(5);

        $ListTypeField = DropdownField::create('ListType', "ListType", $this->dbObject('ListType')->enumValues());
        $ListTypeField->setEmptyString("(select)");
        $ListIconField = DropdownField::create('ListIcon', "ListIcon", $this->dbObject('ListIcon')->enumValues());
        $ListIconField->setEmptyString("(select)");
        $ListIconField->displayIf("ListType")->isEqualTo("ul");
        $NumberStyleField = DropdownField::create('NumberStyle', "NumberStyle", $this->dbObject('NumberStyle')->enumValues());
        $NumberStyleField->setEmptyString("(select)");
        $NumberStyleField->displayIf("ListType")->isEqualTo("ol");
        $TitleCenterField = CheckboxField::create('TitleCenter' , "TitleCenter");
        $ListCenterField = CheckboxField::create('ListCenter' , "ListCenter");

        $fields->addFieldToTab('Root.Main', $TitleCenterField);
        $fields->addFieldToTab('Root.Main', $ContentField);
        $fields->addFieldToTab('Root.Main', $ContentBelowField);
        $fields->addFieldToTab('Root.Main', $ListTypeField);
        $fields->addFieldToTab('Root.Main', $ListIconField);
        $fields->addFieldToTab('Root.Main', $NumberStyleField);
        $fields->addFieldToTab('Root.Main', $ListCenterField);

        if ($this->ID) {

            $ListItemsGridField = GridField::create(
                'ListItems',
                'ListItems',
                $this->ListItems(),
                GridFieldConfig_RecordEditor::create()
                    ->addComponent(new GridFieldOrderableRows())
            );

            //$field = TextField::create('test', 'test');
            $fields->addFieldToTab('Root.Main', $ListItemsGridField);
        }
        return $fields;
    }

    public function getType()
    {
        return 'List';
    }
}