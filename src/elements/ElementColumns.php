<?php

namespace Hestec\ElementalExtensions\Elements;

use DNADesign\Elemental\Models\BaseElement;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
use SilverStripe\Forms\GridField\GridField;
use SilverStripe\Forms\GridField\GridFieldConfig_RecordEditor;
use Symbiote\GridFieldExtensions\GridFieldOrderableRows;
use Hestec\ElementalExtensions\Dataobjects\ContentColumn;
use SilverStripe\Forms\CheckboxField;

class ElementColumns extends BaseElement
{

    private static $table_name = 'HestecElementColumns';

    private static $singular_name = 'Column';

    private static $plural_name = 'Columns';

    private static $description = 'Element with multiple columns';

    private static $icon = 'font-icon-columns';

    private static $db = [
        'Content' => 'HTMLText',
        'TitleCenter' => 'Boolean'
    ];

    private static $has_many = array(
        'ContentColumns' => ContentColumn::class
    );

    private static $inline_editable = false;

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();

        $ContentField = HTMLEditorField::create('Content', "Content");
        $ContentField->setRows(5);
        $TitleCenterField = CheckboxField::create('TitleCenter' , "TitleCenter");

        $fields->addFieldToTab('Root.Main', $TitleCenterField);
        $fields->addFieldToTab('Root.Main', $ContentField);

        if ($this->ID) {

            $ContentColumnsGridField = GridField::create(
                'ContentColumns',
                'ContentColumns',
                $this->ContentColumns(),
                GridFieldConfig_RecordEditor::create()
                    ->addComponent(new GridFieldOrderableRows())
            );

            //$field = TextField::create('test', 'test');
            $fields->addFieldToTab('Root.Main', $ContentColumnsGridField);
        }
        return $fields;
    }

    public function getType()
    {
        return 'Columns';
    }
}