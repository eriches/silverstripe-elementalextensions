<?php

namespace Hestec\ElementalExtensions\Elements;

use DNADesign\Elemental\Models\BaseElement;

class ElementChildren extends BaseElement
{

    private static $table_name = 'HestecElementChildren';

    private static $singular_name = 'Children';

    private static $plural_name = 'Children';

    private static $description = 'Children links';

    private static $icon = 'font-icon-page-multiple';

    private static $db = [
    ];

    public function getType()
    {
        return 'Children';
    }

}