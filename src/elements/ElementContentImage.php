<?php

namespace Hestec\ElementalExtensions\Elements;

use DNADesign\Elemental\Models\BaseElement;
use SilverStripe\CMS\Model\SiteTree;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
use SilverStripe\Forms\OptionsetField;
use SilverStripe\Forms\TextField;
use SilverStripe\Forms\DropdownField;
use SilverStripe\Assets\Image;
use SilverStripe\AssetAdmin\Forms\UploadField;
use SilverStripe\Forms\TreeDropdownField;
use SilverStripe\Forms\CheckboxField;
use SilverStripe\Forms\HeaderField;

class ElementContentImage extends BaseElement
{

    private static $table_name = 'HestecElementContentImage';

    private static $singular_name = 'Content & Image element';

    private static $plural_name = 'Content & Image elements';

    private static $description = 'Adds a Content & Image element';

    private static $icon = 'font-icon-image';

    private static $db = [
        'Content' => 'HTMLText',
        'ContentAlign' => "Enum('start,center,end','start')",
        'ContentValign' => "Enum('start,center,end','start')",
        'Caption' => 'Varchar(255)',
        'ImagePosition' => "Enum('left,right', 'left')",
        'ImageAlign' => "Enum('start,center,end','center')",
        'ImageValign' => "Enum('start,center,end','center')",
        'ImageBorder' => 'Boolean',
        'ImageStyle' => "Enum('rounded,rounded-circle','')",
        'LinkExtern' => 'Varchar(255)',
        'LinkText' => 'Varchar(255)',
        'LinkTitle'  => 'Varchar(255)',
        'LinkAlign' => "Enum('LEFT,CENTER,RIGHT','LEFT')",
        'LinkFaIcon' => 'Varchar(25)',
        'LinkFaIconPos' => "Enum('LEFT,RIGHT','LEFT')",
        'LinkButton' => 'Boolean',
        'Border' => 'Boolean',
        'ImageMobileHide' => 'Boolean'
    ];

    private static $defaults = array(
        'Border' => true
    );

    private static $has_one = array(
        'Image' => Image::class,
        'LinkIntern' => SiteTree::class
    );

    private static $owns = [
        'Image'
    ];

    public function getCMSFields()
    {

        $fields = parent::getCMSFields();

        $ContentHeaderField = HeaderField::create('ContentHeader', "Content");
        $ContentField = HTMLEditorField::create('Content', "Content");
        $ContentField->setRows(5);
        $ContentAlignField = DropdownField::create('ContentAlign', "ContentAlign", $this->dbObject('ContentAlign')->enumValues());
        $ContentAlignField->setDescription("Horizontal position of content");
        $ContentValignField = DropdownField::create('ContentValign', "ContentValign", $this->dbObject('ContentValign')->enumValues());
        $ContentValignField->setDescription("Vertical position of content");

        $LinkHeaderField = HeaderField::create('LinkHeader', "Link");
        $LinkInternField = TreeDropdownField::create('LinkInternID', "LinkIntern", SiteTree::class);
        $LinkExternField = TextField::create('LinkExtern', "LinkExtern");
        $LinkTextField = TextField::create('LinkText', "LinkText");
        $LinkTitleField = TextField::create('LinkTitle', "LinkTitle");
        $LinkAlignField = DropdownField::create('LinkAlign', "LinkAlign", $this->dbObject('LinkAlign')->enumValues());
        $LinkFaIconField = TextField::create('LinkFaIcon', "LinkFaIcon");
        $LinkFaIconPosField = DropdownField::create('LinkFaIconPos', "LinkFaIconPos", $this->dbObject('LinkFaIconPos')->enumValues());
        $LinkButtonField = CheckboxField::create('LinkButton', "LinkButton");
        $BorderField = CheckboxField::create('Border', "Border");
        $ImageMobileHideField = CheckboxField::create('ImageMobileHide', "ImageMobileHide");

        $ImageHeaderField = HeaderField::create('ImageHeader', "Image");
        $ImageField = UploadField::create('Image', "Image");
        $ImageField->setAllowedMaxFileNumber(1);
        $ImagePositionField = OptionsetField::create('ImagePosition', "ImagePosition", $this->dbObject('ImagePosition')->enumValues());
        $ImagePositionField->setDescription("Image postition left or right from the text.");
        $ImageAlignField = DropdownField::create('ImageAlign', "ImageAlign", $this->dbObject('ImageAlign')->enumValues());
        $ImageAlignField->setDescription("Horizontal position of image");
        $ImageValignField = DropdownField::create('ImageValign', "ImageValign", $this->dbObject('ImageValign')->enumValues());
        $ImageValignField->setDescription("Vertical position of image");
        $ImageStyleField = DropdownField::create('ImageStyle', "ImageStyle", $this->dbObject('ImageStyle')->enumValues());
        $ImageStyleField->setEmptyString("(optional");
        $ImageBorderField = CheckboxField::create('ImageBorder', "ImageBorder");

        $CaptionField = TextField::create('Caption', "Caption");

        $fields->addFieldToTab('Root.Main', $ContentHeaderField);
        $fields->addFieldToTab('Root.Main', $ContentField);
        $fields->addFieldToTab('Root.Main', $ContentAlignField);
        $fields->addFieldToTab('Root.Main', $ContentValignField);
        $fields->addFieldToTab('Root.Main', $ImageHeaderField);
        $fields->addFieldToTab('Root.Main', $ImageField);
        $fields->addFieldToTab('Root.Main', $ImagePositionField);
        $fields->addFieldToTab('Root.Main', $ImageAlignField);
        $fields->addFieldToTab('Root.Main', $ImageValignField);
        $fields->addFieldToTab('Root.Main', $LinkHeaderField);
        $fields->addFieldToTab('Root.Main', $LinkInternField);
        $fields->addFieldToTab('Root.Main', $LinkExternField);
        $fields->addFieldToTab('Root.Main', $LinkTextField);
        $fields->addFieldToTab('Root.Main', $LinkTitleField);
        $fields->addFieldToTab('Root.Main', $LinkAlignField);
        $fields->addFieldToTab('Root.Main', $LinkFaIconField);
        $fields->addFieldToTab('Root.Main', $LinkFaIconPosField);
        $fields->addFieldToTab('Root.Main', $LinkButtonField);
        $fields->addFieldToTab('Root.Main', $ImageField);
        $fields->addFieldToTab('Root.Main', $CaptionField);
        $fields->addFieldToTab('Root.Main', $ImagePositionField);
        $fields->addFieldToTab('Root.Main', $ImageStyleField);
        $fields->addFieldToTab('Root.Main', $ImageBorderField);
        $fields->addFieldToTab('Root.Main', $ImageMobileHideField);
        $fields->addFieldToTab('Root.Main', $BorderField);

        return $fields;

    }

    public function getType()
    {
        return 'Content & Image';
    }
}