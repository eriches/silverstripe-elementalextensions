<?php

namespace Hestec\ElementalExtensions\Elements;

use DNADesign\Elemental\Models\BaseElement;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
use SilverStripe\Forms\GridField\GridField;
use SilverStripe\Forms\GridField\GridFieldConfig_RecordEditor;
use Symbiote\GridFieldExtensions\GridFieldOrderableRows;
use SilverStripe\Forms\DropdownField;
use Hestec\ElementalExtensions\Dataobjects\TimelineItem;
use SilverStripe\Forms\OptionsetField;

class ElementTimeline extends BaseElement
{

    private static $table_name = 'HestecElementTimeline';

    private static $singular_name = 'Timeline';

    private static $plural_name = 'Timelines';

    private static $description = 'Element with timeline';

    //private static $icon = 'timeline-icon';
    private static $icon = 'font-icon-calendar';

    private static $db = [
        'Content' => 'HTMLText',
        'TimelineType' => "Enum('LEFT,MIDDLE','MIDDLE')",
        'MiddleStart' => "Enum('LEFT,RIGHT','RIGHT')"
    ];

    private static $has_many = array(
        'TimelineItems' => TimelineItem::class
    );

    private static $inline_editable = false;

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();

        $ContentField = HTMLEditorField::create('Content', "Content");
        $ContentField->setRows(5);

        $TimelineTypeField = OptionsetField::create('TimelineType', "TimelineType", $this->dbObject('TimelineType')->enumValues());
        $MiddleStartField = OptionsetField::create('MiddleStart', "MiddleStart", $this->dbObject('MiddleStart')->enumValues());

        $fields->addFieldToTab('Root.Main', $ContentField);
        $fields->addFieldToTab('Root.Main', $TimelineTypeField);
        $fields->addFieldToTab('Root.Main', $MiddleStartField);

        if ($this->ID) {

            $TimelineItemsGridField = GridField::create(
                'TimelineItems',
                'TimelineItems',
                $this->TimelineItems(),
                GridFieldConfig_RecordEditor::create()
                    ->addComponent(new GridFieldOrderableRows())
            );

            //$field = TextField::create('test', 'test');
            $fields->addFieldToTab('Root.Main', $TimelineItemsGridField);
        }
        return $fields;
    }

    public function getType()
    {
        return 'Timeline';
    }
}