<?php

namespace Hestec\ElementalExtensions\Elements;

use DNADesign\Elemental\Models\BaseElement;
use Sheadawson\DependentDropdown\Forms\DependentDropdownField;
use SilverStripe\AssetAdmin\Forms\UploadField;
use SilverStripe\CMS\Model\SiteTree;
use SilverStripe\Forms\CheckboxField;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
use SilverStripe\Forms\LiteralField;
use SilverStripe\Forms\OptionsetField;
use SilverStripe\Forms\TextField;
use Hestec\ElementalExtensions\Dataobjects\FaqQuestion;
use SilverStripe\Forms\GridField\GridField;
use SilverStripe\Forms\GridField\GridFieldConfig_RecordEditor;
use Symbiote\GridFieldExtensions\GridFieldOrderableRows;
use Hestec\FaqPage\FaqCategory;
use Hestec\FaqPage\FaqPage;
use SilverStripe\Forms\DropdownField;
use SilverStripe\Forms\NumericField;
use SilverStripe\Assets\Image;

class ElementFaq extends BaseElement
{

    private static $table_name = 'HestecElementFaq';

    private static $singular_name = 'Faq';

    private static $plural_name = 'Faqs';

    private static $description = 'Adds a faq';

    //private static $icon = 'faq-icon';
    private static $icon = 'font-icon-white-question';

    private static $inline_editable = false;

    private static $db = [
        'IntroText' => 'HTMLText',
        'NumberOfQuestions' => 'Int',
        'ImagePosition' => "Enum('left,right', 'right')",
    ];

    private static $has_one = array(
        'FaqPage' => FaqPage::class,
        'FaqCategory' => FaqCategory::class,
        'Image' => Image::class,
    );

    private static $owns = [
        'Image'
    ];

    private static $has_many = array(
        'FaqQuestions' => FaqQuestion::class
    );

    public function getCMSFields()
    {

        $this->beforeUpdateCMSFields(function ($fields) {

            $datesSource = function($val) {

                $output = FaqCategory::get()->filter('PageID', $val)->map('ID', 'Title');

                return $output;

            };

            $fields->removeByName('FaqQuestions');

            $IntroTextField = HTMLEditorField::create('IntroText', _t('Element.INTRO_TEXT', "Intro text"));
            $IntroTextField->setRows(5);

            $infotext = "You can include the questions of a category of an FAQ page in this FAQ element. Choose the page and the category and the questions of this category will be included above the questions of this element.";

            $FAQCategorieInfoField = LiteralField::create('FAQCategorieInfoField', "<p>"._t('ElementFaq.FAQCATEGORIEINFO', $infotext)."</p>");

            $FAQPageSource = FaqPage::get()->map('ID', 'Title');

            $FAQPageField = DropdownField::create('FaqPageID', _t('ElementFaq.FAQPAGE', "FAQ page"), $FAQPageSource);
            $FAQPageField->setEmptyString("("._t('Element.SELECT_AN_OPTION', "Select an option").")");

            $FAQCategorieField = DependentDropdownField::create('FaqCategoryID', _t('ElementFaq.FAQCATEGORY', "FAQ category"), $datesSource);
            $FAQCategorieField->setDepends($FAQPageField);
            $FAQCategorieField->setEmptyString("("._t('Element.SELECT_AN_OPTION', "Select an option").")");

            $NumberOfQuestionsField = NumericField::create('NumberOfQuestions', _t('ElementFaq.NUMBEROFQUESTIONS', "Number of questions"));
            $NumberOfQuestionsField->setDescription(_t('ElementFaq.NUMBEROFQUESTIONS_DESCRIPTION', "Number of questions you want to display from the choosen Faq page category (from the first)"));

            $ImageField = UploadField::create('Image', "Image");
            $ImageField->setAllowedMaxFileNumber(1);
            $ImagePositionField = OptionsetField::create('ImagePosition', "ImagePosition", $this->dbObject('ImagePosition')->enumValues());
            $ImagePositionField->setDescription("Image postition left or right from the text.");

            $FaqQuestionsGridField = GridField::create(
                'FaqQuestions',
                _t('ElementFaq.QUESTIONS', "Questions"),
                $this->FaqQuestions(),
                GridFieldConfig_RecordEditor::create()
                    ->addComponent(new GridFieldOrderableRows())
            );

            $fields->addFieldToTab('Root.Main', $IntroTextField);
            $fields->addFieldToTab('Root.Main', $ImageField);
            $fields->addFieldToTab('Root.Main', $ImagePositionField);
            $fields->addFieldToTab('Root.Main', $FAQPageField);
            $fields->addFieldToTab('Root.Main', $FAQCategorieField);
            $fields->addFieldToTab('Root.Main', $NumberOfQuestionsField);
            $fields->addFieldToTab('Root.Main', $FaqQuestionsGridField);

        });

        $fields = parent::getCMSFields();

        return $fields;
    }

    public function RemainNumberOfQuestions(){

        return $this->NumberOfQuestions - $this->FaqQuestions()->count();

    }

    public function getType()
    {
        return 'Faq';
    }
}
