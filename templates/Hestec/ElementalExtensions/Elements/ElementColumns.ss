<div class="container $ExtraClass">
	<% if $ShowTitle %><h2 class="<% if $TitleCenter %>text-center<% end_if %><% if $HSize && $HSize != normal %> $HSize<% end_if %>">$Title</h2><% end_if %>
	<% if $Content %>$Content<% end_if %>
	<% if $ContentColumns %>
        <div class="row pt-3">
			<% loop $ContentColumns %>
                <div class="col-12<% if $Cols %> col-md-$Cols<% else %> col-md<% end_if %> pb-3 pb-md-0<% if not $Last && $BorderRight %> column-border-right<% end_if %>">
					<% if $Card %>
                    <div class="card">
                    <div class="card-body">
					<% end_if %>
					<% if $Icon %><% if $CenterIcon %><div class="d-flex justify-content-center"><% end_if %><img class="img-fluid pb-3" src="$IconSized.URL" alt="$Title"><% if $CenterIcon %></div><% end_if %><% end_if %>
                    <<% if $Header %>$Header<% else %>h2<% end_if %> class="<% if $HSize && $HSize != normal %>$HSize <% end_if %><% if $CenterHeader %>text-center<% end_if %>">$Title</<% if $Header %>$Header<% else %>h2<% end_if %>>
					<% if $CenterContent %><div class="text-center"><% end_if %>$Content<% if $CenterContent %></div><% end_if %>
					<% if $LinkUrlType %><div class="align-self-end text-$LinkAlign.LowerCase"><a<% if $LinkButton %> class="btn-info btn-lg"<% end_if %> href="$LinkUrlType.Url"<% if $LinkUrlType.Type = EXTERN %> target="_blank"<% end_if %><% if $LinkNoFollow %> rel="nofollow noopener"<% end_if %> title="$LinkTitle"><%if $LinkFaIcon && $LinkFaIconPos != RIGHT %><span class="icon $LinkFaIcon"></span> <% end_if %> $LinkText <%if $LinkFaIcon && $LinkFaIconPos = RIGHT %> <span class="icon $LinkFaIcon"></span><% end_if %></a></div><% end_if %>
					<% if $Card %>
                    </div>
                    </div>
					<% end_if %>
                </div>
			<% end_loop %>
        </div>
	<% end_if %>
</div>
